# Description
#    A Hubot script with UserLocal dialogue API
#
# Configuration:
#    USERLOCAL_API_KEY
#
# Author:
#   Zoar

request = require 'request'

module.exports = (robot) ->
   robot.respond /(\S+)/i, (msg) ->
      USERLOCAL_API_KEY = process.env.USERLOCAL_API_KEY
      base_str = "https://chatbot-api.userlocal.jp/api/chat"
      key_str = "?key=" + USERLOCAL_API_KEY
      message = msg.match[1]
      enc_msg = "&message=" + encodeURIComponent(message)
      post = base_str + key_str + enc_msg
      request.post post, (err, res, body) ->
         json = JSON.parse body
         reply_str = json.result
         msg.reply reply_str